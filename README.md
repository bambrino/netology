# devops-netology

Добавление в игнор по маске 
- звездочка на конце .* - все расширения

- звездочка между в имени файла bla-bla.*.bla - файлы bla-bla.ЛЮБОЕ.bla

- двойная звездочка в пути - bla/**/bla.txt - файл bla.txt будет проигнорирован в люблй дочерней 
директории директории bla

- двойная звездочка **/xz15 - такой элемент (в данном случае папка) будут проигнорированы в любом месте
от gitignore

- такой вариант *.txt - будет прогнорированы все файлы с расширением txt в директории уровня gitignore 

